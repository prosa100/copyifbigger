﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopyIfBigger
{
    class Program
    {
        static long GetFileSize(string fileName)
        {
            return new FileInfo(fileName).Length;
        }
        static void Main(string[] args)
        {
            CopyIfBigger(args[0], args[1]);
        }

        static void CopyIfBigger(string srcFileName, string destFileName)
        {
            if (!File.Exists(destFileName) || GetFileSize(srcFileName) > GetFileSize(destFileName))
                File.Copy(srcFileName, destFileName, true);
        }

        //  y + None
        //  y + Smaller
        //  n + Biger
        //  n + Equal

        //static string tempDirectory;
        //static string MakeFile(string name, string contents)
        //{
        //    var path = Path.Combine(tempDirectory,name);
        //    File.WriteAllText(path, contents);
        //    return path;
        //}

        //static void DoTests()
        //{
        //    tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        //    Directory.CreateDirectory(tempDirectory);
        //    Directory.Delete(tempDirectory);
        //}

        //static void Dirversion(string[] args)
        //{
        //    string dirSrc = args[0];
        //    string dirDst = args[0];
        //    foreach (var srcfile in Directory.GetFiles(dirSrc, "*.*", SearchOption.AllDirectories))
        //    {
        //        var destpath = Path.Combine(dirSrc, Path.GetFileName(srcfile));
        //        if (!File.Exists(destpath) || GetFileSize(srcfile) > GetFileSize(destpath))
        //            File.Copy(srcfile, destpath);
        //    }
        //}
    }
}
